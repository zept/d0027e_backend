#!flask/bin/python
from flask import Flask, request, jsonify
from werkzeug import secure_filename
from db import getPictures
import dateutil.parser
import os

app = Flask(__name__, static_url_path='/static')

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLD = './static'
UPLOAD_FOLDER = os.path.join(APP_ROOT, UPLOAD_FOLD)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/getPictures')
def index():
    # Läs in parametrar
    tag1 = request.args.get('fromDate')
    tag2 = request.args.get('toDate')
    tag3 = request.args.getlist('tags')

    imageList = []

    for x in getPictures():

        dateTaken = dateutil.parser.parse(x['date'])
        dateFrom = dateutil.parser.parse(tag1)
        dateTo = dateutil.parser.parse(tag2)

        if ((dateTaken > dateFrom) & (dateTaken < dateTo)):
            for y in x['tags']:
                if y['value'].lower() in tag3:
                    imageList.append(x)

    # Returnera json respons
    return jsonify(imageList)

@app.route('/getTags')
def tags():
    # Läs in parametrar
    tagList= []

    for x in getPictures():
        for y in x['tags']:
            if y['value'] not in tagList:
                tagList.append(y['value'])

    # Returnera json respons
    return jsonify(tagList)

@app.route('/getCredentials')
def login():
    # Läs in parametrar
    response = ""

    # Läs in parametrar
    tag1 = request.args.get('user')
    tag2 = request.args.get('password')

    # Kontrollera att parametrar för både user och password har skickats med.
    if ((tag1 != None) & (tag2 != None)):

        # Om användaren är Ahmed så skicka tillbaka API-kod och roll
        if ((tag1.lower() == "ahmed@gmail.com") & (tag2.lower() == "1234")):
            response = {"loginKey" :"32ifj0ijf849023j",
                        "role": "Photographer"}

        # Om användaren är Anna så skicka tillbaka API-kod och roll
        if ((tag1.lower() == "anna@gmail.com") & (tag2.lower() == "5678")):
            response = {"loginKey" :"53869034dfjkjksf",
                        "role": "User"}

    # Returnera json respons
    return jsonify(response)

@app.route('/upload', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['image']
      f.save(os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(f.filename)))
      print(request.form['caption'])
      print(request.form['date'])
      tagss = request.form.getlist('tags')
      for x in tagss:
          print(x)
      return 'file uploaded successfully'

if __name__ == '__main__':
    app.run(debug=True, port=3100, threaded=True)
