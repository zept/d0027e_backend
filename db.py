import os

def getPictures():

    return [
        {
        "src": "http://192.168.0.200/skola/d0027e/static/1.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/1.jpg",
        "tags" : [
            {"value": "urban", "title": "urban"},
            {"value": "senior", "title": "senior"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "1973-01-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/2.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/2.jpg",
        "tags" : [
            {"value": "nature", "title": "nature"},
            {"value": "skateboard", "title": "skateboard"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2011-01-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/3.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/3.jpg",
        "tags" : [
            {"value": "birds", "title": "birds"},
            {"value": "ball", "title": "ball"},
            {"value": "red", "title": "red"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2019-01-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/4.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/4.jpg",
        "tags" : [
            {"value": "time", "title": "time"},
            {"value": "waste", "title": "wasted"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2019-01-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/5.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/5.jpg",
        "tags" : [
            {"value": "green", "title": "green"},
            {"value": "skull", "title": "skull"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2018-01-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/6.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/6.jpg",
        "tags" : [
            {"value": "tech", "title": "tech"},
            {"value": "reactTags", "title": "reactTags"},
            {"value": "false", "title": "false"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2000-01-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/7.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/7.jpg",
        "tags" : [
            {"value": "elephant", "title": "elephant"},
            {"value": "grey", "title": "grey"},
            {"value": "ox", "title": "ox"},
            {"value": "bison", "title": "bison"},
            {"value": "lion", "title": "lion"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2001-01-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/8.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/8.jpg",
        "tags" : [
            {"value": "faith", "title": "faith"},
            {"value": "temple", "title": "temple"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2014-01-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/9.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/9.jpg",
        "tags" : [
            {"value": "orange", "title": "orange"},
            {"value": "fruit", "title": "fruit"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2019-01-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/10.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/10.jpg",
        "tags" : [
            {"value": "slope", "title": "slope"},
            {"value": "downhill", "title": "downhill"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2017-04-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/11.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/11.jpg",
        "tags" : [
            {"value": "battle", "title": "battle"},
            {"value": "sword", "title": "sword"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2017-03-01T10:00:00.000Z"},
        {
        "src": "http://192.168.0.200/skola/d0027e/static/12.jpg",
        "thumbnail": "http://192.168.0.200/skola/d0027e/static/thumbnails/12.jpg",
        "tags" : [
            {"value": "flower", "title": "flower"},
            {"value": "bee", "title": "bee"}
            ],
        "thumbnailWidth": 345,
        "thumbnailHeight": 230,
        "caption": "What is this picture?",
        "date": "2017-02-01T10:00:00.000Z"}
        ]
